<div align="right" >
<p align="right" >
<img src="https://raw.githubusercontent.com/OpeningDesign/OD_Marketing/master/Logos/od_icon_logo_2.jpg" width="120px"/>
</p>
</div>

<!--
<div align="right" >
<p align="right" >
<img src="https://dl.dropbox.com/s/64fehzmn6sfaq1g/PHD%20logo.png?dl=1" width="300px"/>
</p>
</div>
-->

### Proposal for [OpeningDesign's](http://openingdesign.com/) Architecture<!-- & Structural Engineering& Interior Design Engineering--> Services

<!--

### Proposal for the following services<!--[OpeningDesign's](http://openingdesign.com/) 
- Interior Design
- Furniture, Fixtures, and Equipment (FF&E) Selection and Procurement
- Architecture and Engineering

-->

---

FOR:

Tim Wick
Wick Buildings
405 Walter Rd
Mazomanie, WI 53560

---



Hi Tim,

Thank you for the chat the other day. We’re excited to have the opportunity to share the following proposal.

Although a more nuanced list of requirements will undoubtedly unfold as the project evolves, on a high-level, I understand that this project will include the following list of requirements/priorities.

- New building, for Brunsell Lumber, will be approximately 200ft x 160ft with a mezzanine.
- Will be a post frame building supplied by Wick Building, along with structural engineering
- Will be used for storage of wood components initially, but 2nd phase will include manufacturing of prefabricated wood wall panels.
	- This proposal includes the layout of the offices and manufacturing equipment, but doesn't not assume that this layout will be submitted in the initial permit submission. 

<!--

>The above list is also included in the following online document... 
https://docs.google.com/document/d/125-xQcU_22aZwW-1699Hmp7ui3n38U7tFa5BBfvOMRc/edit
>
>Feel free, at any time, to add/modify this document if you have additional thoughts.  This will be a living document throughout the duration of the design. 
-->

<br>

The following is a breakdown of services and phases we anticipate for your project.

---


## Phases & Scope of Services

### **Phase 1 - Programming** <!--*(Fees associated with this phase are discounted at 25%)*-->
  
  <!--* Architecture-->
  
  * Conduct zoning & building code analysis
  * This proposal does not include a site survey-which we will need for this project.  We assume a survey of the existing site (CAD format) will by supplied by a 3rd party surveyor. 
* At the end of this phase, we provide up to (2) diagrammatic floor plan options which will explore ideal layout and adjacencies of the various spaces throughout the project, as well, as their relation to the site.  These design options will address varying approaches in the following criteria.
    * Functional relationship of the various spaces
    * Code restrictions/requirements
    * Structural considerations
    * Mechanical considerations
    * Major equipment locations
    * Plumbing considerations
    * Environmental factors
    * Optimizing Daylight
    * Acoustical considerations
    * Existing site layout
    * Furniture/equipment requirements
  * A few examples of these diagrammatic floor plans
    * [Example 1](https://www.dropbox.com/s/bblk8t8dmqsiflz/20211104%20-%20101_W_33rd_St%20-%20Site%20Layout%20Options.pdf?dl=1)
    * [Example 2](https://www.dropbox.com/s/aehy9hh5f2ih75b/20211118%20-%20Kegonsa_East%20-%20Layout%20Options%20and%20Existing%20Floor%20Plans.pdf?dl=1)
   <!--* Measure, model, and draft up the existing conditions relative to the scope of work
	   * If there's existing as-built drawings of the space, less effort would be necessary for this phase.  Even less so, if there are CAD-based drawings.  Although hardcopy (paper) drawings will also be helpful.  Either way, however, we will still need conduct site measurements to verify dimensions.
  * This proposal does not include a site survey-which we will need for this project.  We assume a survey of the existing site (CAD format) will by supplied by a 3rd party surveyor. 
    * Ideally, the survey would include:
      * locations of all structures on property
      * location of impervious areas
      * property lines with metes and bounds
      * recorded easements
      * utility locations 
        * inside the site
        * and in the adjacent right of way (streets)
      * topographical elevations
      * locations of large trees
      * locations of existing houses/structures on adjacent properties (used to determine allowed lakeshore setbacks)-->


<!--

        
            * As mentioned, we more than likely will have to show the city the basement layout as well, so they have to assess that the restrooms are accessible.


* Civil Engineering *(see attached proposal for details)*
        * Boundary Survey and Title Analysis
        * Topographic Collection
        * Existing Site Plan
        * Soils Analysis (Infiltration) 

    * This is the phase where we help you define and refine the problem--that is, we help you tease out the list of problems you would like to solve with this renovation.  The online, real-time xxx, mentioned above, will be the location where both parties can log your list of requirements and prioritizes.


Although not crucial,  in these earlier phases it's best practice to supply an official survey of the site, as early as possible as it assures the proposed design, as it is laid out on the site, is based on accurate information.  It eliminates the potential for any surprises and rework further down the line.

-----------------------------------------------------------------------------------------------------------------------
    * Civil
        * Assemble and compose site survey
            * Elevation contours
            * Property Lines
            * Document existing site features
            * Document setbacks/easements
            * Locate existing utilities 
                * Water
                * Sewer
                * Electrical

-----------------------------------------------------------------------------------------------------------------------
        * Conduct research on list of spaces/rooms 

-----------------------------------------------------------------------------------------------------------------------

        * Conduct structural assessment    
    * We can provide a survey of the property, but this proposal assumes one will be provided
    * Provide quick, single-line, site studies and floor plan diagrams 


-----------------------------------------------------------------------------------------------------------------------

    * LEED
        * Facilitate workshop with owner and the various consultants/contractors to determine [LEED credits](https://github.com/OpeningDesign/OD_Library/blob/master/Green/LEED%20Credits%20-%20New%20Construction%20-%20LEED%20v4.md) to prioritize 
        * Register project with the U.S. Green Building Council’s (USGBC’s) Leadership in Energy and Environmental Design (LEED) certification program

-->



### **Phase 2 - Schematic Design**
  
* During this phase we will start developing what the look and feel of the project will be.  We will provide up to (2) design options presented in the following formats
 * Architecture<!--& Interior Design-->
     * Site Plan
     * Floor Plans
     * Interior Elevations, if necessary (key areas only)
     * Building Sections
     * Elevations
	     <!--* Of the scope area only, not the entire building-->
	  * 3-dimensional renderings/animations
	       * [Here's](https://www.youtube.com/playlist?list=PL35WBXQO6nuCUDa5jHtURHCcOp3jXOrtc) a couple examples of past animations we've done, as well as renderings, [here](http://openingdesign.com/rendering_examples/).
      * Start coordinating and eliciting feedback from the various subs and/or engineering disciplines. 
       * Will begin to suggest a palette of exterior<!--/interior--> materials <!--(wall, flooring, ceiling, and millwork, etc.)-->
      <!--* Precedent studies and mood boards
        * Furniture, Fixtures, and Equipment (FF&E)
    - Floor plans will take initial stabs at suggested furniture layout-->


<!--

 -----------------------------------------------------------------------------------------------------------------------

       * Provide a schematic/preliminary estimate for cost of construction or help GC with quantity takes-offs

-----------------------------------------------------------------------------------------------------------------------

    * LEED
        * Conduct energy modeling analysis to determine ideal orientation, shading strategies and glazing percentages to maximum energy efficiency
        * Compose and distribute **LEED Certification Plan** to consultants/contractors.  Includes.
            * LEED points targets
            * Suggested strategies for implementing
            * Roles and responsibilities of the various parties
            * Schedule
            * List of system/component to be certified
            * List of documentation required for submissions

-----------------------------------------------------------------------------------------------------------------------

-->

### **Phase 3 - Design Development**

   * Here we<!--, along with our consultants,  --> will dial into one design by either refining one of the proposed designs and/or combining the desired aspects of the other design(s) proposed.
      * Architecture<!-- & Interior Design-->
          * Site Plan
	          * Will use supplied 3rd party survey
	          <!--* Will include landscaping plan for city review-->
          <!--* Site Plan, if necessary for possible zoning or plan commission review 
          * Does not include, however, an official survey with metes and bounds.
	          * Will coordinate with your civil engineer-->
           * Floor Plans
           * Demolition plans, if applicable
           * Exterior Elevations
               <!--* Of the scope area only, not the entire building-->
           * Interior Elevations - key areas only
           * Building Sections
           * 3D renderings and animations
           * Suggest and refine the palette of finish materials. <!-- (wall, flooring, ceiling, and millwork, etc.)-->
               * Will provide material boards with physical samples for review
           * Continue to coordinate and eliciting feedback from the various subs and/or engineering disciplines
       <!--- Furniture, Fixtures, and Equipment (FF&E) 
           - Will continue to refine indoor furniture section
           - Will start coordinating with furniture vendors-->
   
	   * At the end of this phase we should have sufficient documentation to achieve any type of local zoning and/or plan commission approvals that may be necessary. *Please note, this does not include the building permit, which will be applied for in later stages.* 
	       <!--* For this project, we do not anticipate any zoning and/or plan commission approvals.-->
	       * Please note, if these AHJ meetings are necessary, this proposal assumes one attempt at applying for a city approval. If a redesign is required, and/or additional applications are necessary, we can either charge hourly for our services, or you can request we provide an adjusted proposal before moving forward.
   
   
       <!--* Civil Engineering (see attached proposal for details)
            * Grading and Erosion Control Plan
            * Shoreland Zoning Permit Application
            * Shoreland Erosion Control & Mitigation Report 
            * Vegetative Buffer Plan (If required)-->

---
<!--
> ##### Early General Contractor(GC) Involvement
> 
> At the end of this phase we highly recommend sharing these drawings with your preferred contractor to get preliminary estimates.  Although the drawings are still somewhat 'schematic' at this stage, they will be developed enough for your contractor to provide a ballpark estimate.  Although this estimate will have a margin of error, being based on drawings that are not 100% complete, with an experienced general contractor, it should be accurate enough to determine if the planned scope is financially feasible and if so, whether it makes sense to move forward developing and detailing out the design in the more time intensive 4th phase--Construction Documents.
> 
> Also, by bringing on an experienced GC (and their subs potentially) very early on in the process, they can provide valuable insight in terms of local supply chains, and local construction conventions, which can help value engineer the project and reach target costs early on in the project.  Put simply, it's easier to change the design earlier on in the process, verses value engineering a project that is fully detailed out in the later stages of the design. 

-->

<!-->Having been in the business for many years, we would be happy to suggest a list of contractor we trust and love working with.  Just let us know.-->

<!--
        *  If necessary, we will help suggest qualified GCs to help with the estimates.

         * Civil Engineer
         * Structural Engineer
         * Mechanical Engineer
         * Electrical Engineer
         * Plumbing Engineer
         * Lighting Designer
         * Kitchen consultant


        * Civil 
            * *Some scope items might not be necessary depending on the municipality requirements*
               * Storm water management/calculations
               * Grading and erosion control plan/details
                * Site stabilization details/methods.
                * Sediment control measures
                <!--* Fire lane plan
                * Utility Plans
                    * Water
                    * Sewer
                    * Storm
                    * Fire Hydrant

-----------------------------------------------------------------------------------------------------------------------
    * LEED
        * Modify and update **LEED Certification Plan** as appropriate
        * Provide shortlist of potential green building products and materials

-----------------------------------------------------------------------------------------------------------------------

    * At the end of this phase we shall have sufficient documentation to apply for a zoning variance.



-->


### **Phase 4 - Construction Documents**

   * Out of all the phases listed in this proposal, the **Construction Document** phase is the most labor intensive.  This is the phase where we dial into the exacting details of the design.  We, along with our consultants, propose to provide the following construction documents for your project.
  
      * Architecture & Interior Design
          * Code summary
          * ADA requirements
          * Life safety plan
          * Emergency and exit sign layouts
          * Site Plan
             * Will be based on information supplied by 3rd party survey
             <!--* Site plan w/ topography, if elevations were supplied by surveyor
             * This assumes the city does not need an official survey for the small amount of worked planned outside-->
          * Demolition plans
          * Floor plans
          * Roof Plan
          * Reflected ceiling plans
	          * Will suggest lighting layout locations
	          <!--* Will work with the GC's electrician to specify other utility fixtures-->
	          *  Does not include switching/lighting controls
	          <!--* Will help pick out decorative fixtures-->
		       <!--* We assume all new lighting will match existing
                   * Does not include picking out fixtures-->
          * Finish Plans
          * Building Sections
          * Building Elevations
              <!--* Of the scope area only, not the entire building-->
          * Wall Sections
          * Stair sections, if necessary
          * [Large scale](http://openingdetail.com/gallery/) exterior/interior details, as necessary
          * Interior elevations of key areas only
	          * millwork/casework elevations
           <!--* Does not include millwork/casework elevations. We assume these will be design/built directly with the GC's or owner's vender.
              * Includes millwork/casework elevations w/ proposed finishes
                  * but does not include [large scale](http://openingdetail.com/gallery/) detailing of these components as we assume we will refine the details in coordination with the the casework contractor
               * We will include, however, elevations of ADA restroom layouts. assume all casework will match existing units
               We assume all casework will match existing. Will include, however, elevations of ADA restroom layouts, if necessary.-->
          * Schedules
              * Door Schedule<!-- and door hardware schedule-->
                  * does not include hardware, we will work with the GC's hardware vendor to dial in on specifics.
              * Window schedule<!--, if necessary-->
              * Finish schedule
                   <!--* We assume as majority of finishes will match existing-->
          * Energy code check, if necessary (COMcheck/REScheck for example)
          <!--* General specifications-->
  
      <!--* Structural
	      * Floor plans with structural member sizes
	      * Structural calculations, if necessary
	       * Foundation Plan, if necessary
	       * We assume, however, as is typical with metal buildings, that the metal building manufacturer will submit all the necessary permit drawings and calculations for all the structural steel in the building.
	           * Does not include sizing of pre-engineered floor/roof trusses. These are usually sized by the building supplier through the GC.-->

       * At the end of this phase we should have sufficient documentation to submit for permit.  Often times, however, due to accelerated construction schedules, we will submit for permit a portion of the way through this phase, and complete the remaining documentation after the permit has been issued.


<!--

        * Structural
             * Specifications
             * Our assumption is that we will work closely with your contracted, or in-house structural engineer.  With their input and guidance, however, we will provide all the necessary drafting
    
    
        * Furniture, Fixtures, and Equipment (FF&E)
            * Finalize furniture selection, specification, and layout
            * Work with the various furniture vendors to finalize order and delivery schedule
            * Selection, specification and procurement of artwork, décor items, and art framing.-->



---

<!--

>##### Level of Detail
>Please note, the more detailed the construction documents are, the 1) the more accurate the GC's numbers will be and 2) the less surprises there will be as to what finally gets built.  However, as one can imagine, the more detailed the drawings, the higher the design fees and the longer it takes to develop them.
>
>In this light, as with most construction projects we have worked on, our clients end up dialing into a balance. That is, spending *just enough* on design fees to get a bid that is pretty accurate -- with the understanding that some of the details will be worked out with the contractor(s) and OpeningDesign along the way.  This is especially true when working with a GC you trust very early in the process. 
>
>If, however, you prefer more thorough documentation, please let us know--as we will adjust this proposal in that regard.
>

-->
<!--
* Mechanical
            * Heating/cooling load calculations
            * Floor plans locating and sizing of
                * Equipment
                * Ductwork
                * Natural gas
            * Air terminal & damper schedules
            * Kitchen equipment hookup
            * Typical details
            * Energy code check
            * Specifications
            * Services not included:
                * Extensive energy modeling beyond prescriptive requirements
           * Electrical
            * Major Equipment Locations
            * Receptacle locations
            * Kitchen equipment hookup
            * Specifications
            * Services not included:
                * Low voltage design
                * Telecom design
                * audio/visual design
                * Security system
                * Fire alarm
                * Process equipment
        * Lighting
            * Circuiting
            * Layout
            * Fixture Selection
            * Site utilities
            * Site Lighting
            * Services not included:
                * Photometric Calculations
                * Lighting controls
                * Daylight modeling
          * Plumbing
            * Floor plans locating and sizing of
                * Equipment
                * Sanitary drainage and venting
                * Water distribution (hot/cold)
            * Plumbing risers
            * Kitchen rough-in sizes    
            * Site utilities 
            * Specifications
            * Services not included:
                * Fire protection (sprinkler) design
                * Process equipment hookup

<!--   


--------------------

        * Electrical/Low Voltage
            * Will provide electrical/low voltage layout, but does not include electrical engineering such as:
            * Branch circuit layouts & panel scheduling
            * Control panel/room layout & sizing
            * Electrical diagrams



-----------------------------------------------------------------------------------------------------------------------

            * Landscape plan
                * Proposed plantings and ground treatment

-----------------------------------------------------------------------------------------------------------------------

            *  Kitchen Equipment Hookup
            * Medical Piping

-----------------------------------------------------------------------------------------------------------------------

         * Commerical Kitchen
            * Floor Plans
            * Equipment List

-----------------------------------------------------------------------------------------------------------------------

      * Low Voltage Plan
        * Fire Alarm
        * Security Systems (if necessary)
        * Audio/Visual
        * Fire Protection
            * Sprinkler system (performance specifications only)
                * detailed fire suppression system is assumed to be provided by FP contractor 
            *  Sprinkler System (Detailed) * Hydraulic Calculations -    

-----------------------------------------------------------------------------------------------------------------------


        * LEED
            * Finalize green building products and materials
            * Develop 'green' construction specifications
            * Provide appropriate submittals and calculations for USGBC's *design phase* application
        * All disciplines
            * Provide construction specifications

-----------------------------------------------------------------------------------------------------------------------


-->

### **Phase 5 - Bidding and Issuing for Permit**


<!-- 
    * In the event you have yet to contract with a general contractor *(which we don't recommend waiting this long to engage with a GC for reasons noted above)*, we will facilitate a more formal bidding process in this phase.  That is, if deemed necessary to get additional bids, we will help distribute the final construction documents to your list of preferred general contractors. We are also happy to help you shortlist a number of qualified contractors as well.
    * * If necessary, a bidding set to a predetermined list of GCs
  -->
    

  * Answer GC & subcontractor's bid questions, issue clarifications
  * Issue the drawings for building permit review
    * Provide the city/town/village, reviewing the drawings, with any additional drawings and/or clarifications they may request after their initial review.  This happens quite often and is a normal part of the process.

> ##### Post Permit Changes
> 
> There are times our clients will make changes relatively late in the process (Phase 4 and beyond). If small enough and done early enough, we can usually subsume them into the base bid.  However, if they become big enough and too late in the process, we will let you know and issue a not-to-exceed change order, for your approval, before any work is done.  
> 
> If you decide at a later time to reissue for a revised permit with these changes, any additional work required by the AHJ for this revised permit is not included in the base bid and will be charged at an hourly rate per the enclosed billing rate schedule.
> 
> Although we will make every effort to make these post-permit changes code compliant, if you instruct your GC to implement these changes before receiving the revised permit, OpeningDesign can not assume any liability for any rework that might result from drawings that are not code compliant upon re-review.  Since the building code is multifaceted and, at times, open to local interpretation, we require the approval of the governing body (AHJ) to manage our risk of liability.  **Please note, this applies to any construction done before the first permit issuance, as well.**

<!--

* Review and award bids

-->

### **Phase 6 - Construction Administration**

* Architecture & Interior Design
	* Site visits
		* Assuming 1 every 4 weeks (additional site visits, if necessary, will be billed at hourly rates outlined in this proposal, including mileage costs)
	* Review/Respond to the following GC inquiries
      * RFIs (request for information)
      * Shop drawings and submittals
      * Material/Product substitutions proposed by the GC
      * Change orders
      * Develop punch list, if necessary
       

<br>

> ##### Termination
> When construction starts, our job is not over.  Our involvement during this phase is typically just as important as the earlier phases, as quite often, due to accelerated schedules, we continue to work out the remaining details of the design.
> 
> In addition, our periodic on-site observation allows us to catch any construction errors and/or quality problems.
> 
>  Having said this, you are free to stop our services at any time, and at any phase, however, as our involvement is crucial in all the above phases, if you terminate our service before the end of construction, we would be required to remove our name as the architect/engineer of record from the AHJ, and would no longer be professionally liable for any errors and omissions that might be exposed later in the project.  Our involvement throughout the project is crucial to minimizes our liability, as well as yours.


<!--

---

    * Civil Engineering (see attached proposal for details)
    * House Offset Staking 
    * Boat House Offset Staking
    * As-Built Stormwater Management Certification
    * As-built House & Boat House Siting Certification (If required) 



       * Considering the site is quite far away from our home offices, site visits are not included in the proposal.  We will, however, conduct site visits upon request—billing rates and travel costs will apply.

    * LEED
        * Review and address any USGBC feedback from *design phase* application
        * Develop construction waste management plan
        * Assist GC with LEED tracking template
        * Provide appropriate submittals and calculations for USGBC's *construction phase* application
        * Review submittals for LEED compliance
        * Prepare a Final LEED Certification Report that documents
            * LEED rating achieved
            * Documentation submitted
            * USGBC reviews
            * LEED Certification Reviews

-->

---

## Services *not* included:

Although **we can provide** the following services, we assume either they are not necessary or will be provided by a 3rd party via the GC or directly contracted through you.

<!--Although we can provide services in both Tiers, for those services in the **1st Tier**, we can provide the most value specific to your project. -->

*Please lets us know if you would like us to include any of the following.*

<!--* ## 1st Tier-->
  * Structural Engineering
  * Civil engineering
    <!--* If the percentage of impervious surfaces on the site is proposed to go above Dane County's prescriptive requirements, we will need to consult with a civil engineer to development the following drawings/calculations-->
       * Storm water management/calculations
       * Grading and erosion control plan/details
        * Site stabilization details/methods.
        * Sediment control measures
        * Retaining wall design
  * HVAC/Mechanical Engineering
	  <!--* We will, however, suggest routing of major ductwork-->
  * Electrical Engineering
	  <!--* Lighting--><!--* ## 2nd Tier-->
  * Plumbing Engineering
  * Food Service or Commercial Kitchen Design
    * Kitchen equipment procurement
     * We will, however, suggest a schematic kitchen layout intended to be refined by a 3rd party kitchen vendor.
  * Furniture, Fixtures & Equipment (FF&E) services
	  <!--*Interior design
	  Picking out interior finishes
	  Casework/millwork elevations-->
  * [Large scale](http://openingdetail.com/gallery/) casework/millwork details
  * [Large scale](http://openingdetail.com/gallery/) interior details
  * Long form (book) construction specifications
  * Landscape design
  * Structural work associated with shoring during construction
  * Land Surveying
  * Low Voltage Design
  * Audio/Visual Design
  * Extensive energy modeling beyond prescriptive requirements
  * LEED Design
  * LEED Commissioning
  * Hyper-Realistic Renderings
  * Detailed Cost Estimation
  * Fire Alarm
  * Fire Protection (Sprinklers)
  * Security
  * Signage Design/Layout
  * Acoustical Engineering Services
  * Geotechnical Engineering
  * Environmental Studies and Reports
  * Materials testing or inspections
  * Information Technology
  * Legal Survey
  * Closeout Record Documents
  * Testing and Balancing of Installed Equipment
  * Environmental Studies
  * Commissioning Services
  * Moving Coordination
  * Post-occupancy Elevation/Studies
  * Maintenance and Operational Programming
  * Building Maintenance Manuals
  * Post-occupancy Evaluation
  * Extensive decorative finish studies
  * Art selection
  * Artwork Production Services 
  * Additional art commission services may be provided to client and quoted separately


<!--

----------------------------------------------------------------------------------
  * Structural Engineering
	  * As mentioned above, we will provide drafting services
  * Energy code check (COMcheck for example)
  * Lighting
    * Picking out and/or specifying light fixtures
      *  We will help suggest decorative fixtures, however
    * Switching/lighting controls
    * As-built drawings for areas outside the scope.
	    * Sometimes an AHJ will request additional floor plans outside the scope area
    * Reflected ceiling plan
    * [Large scale](http://openingdetail.com/gallery/) (3" or 6" = 1'-0") construction details


----------------------------------------------------------------------------------
-->

<!--

## OpeningDesign's Aesthetic Leanings

We have found over the years, one of the hallmarks of a successful and unique project, is when the client and the architect are both vested and excited about the design.

In that regards, to give you a sense of what types of designs we are excited about, please review the following curated resources of architectural designs that have inspired us over the years.

* [Our Pinterest Board for Residential Architecture](https://www.pinterest.com/theoryshaw/-residential-architecture/)
* [Our Pinterest Board for Commercial Architecture](https://www.pinterest.com/theoryshaw/-architecture/)
  
If you are familiar with Pinterest, we would suggest going through these 2 boards and saving those images, to your own board, that resonate with you.  This will further help us dial into a look and feel that aligns with your taste, as well as ours.

Of course, our online [portfolio](http://openingdesign.com/portfolio/) and [gallery](http://openingdesign.com/gallery/) will also give you a sense of our aesthetic as well.

-->

---

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>



## Fees Relative to Desired Workflow

Over the years, OpeningDesign has developed a preferred and unique way of working.  Although OpeningDesign's <!--our--> website's [about page](http://openingdesign.com/about/) provides more detail, in a nutshell, most of our projects are [open source](http://en.wikipedia.org/wiki/Open_source) and are conducted [out-in-the-open](http://openingdesign.com/about/).

This open source concept might sound confusing, as it's a relatively new way of working in our industry, but this open way of working allows us to more agilely share work with our core group of independent architects and designers that we have worked with over the years.  In addition, this open platform always us to share, and build up a common library of details and drawings.

Simply put, by being able to bring in additional help quicker on project and sharing a common library of drawings, we are able to bring more value to our clients--both in cost and schedule.

We understand, however, some clients might prefer a more traditional approach to practicing architecture and interior design.  In that light, we offer (3) tiered fee options that vary from a fully open approach to a more private approach.

### Approach A

* **Approach (A): A Fully Open Approach** where the construction documentation, location, and the names of the parties involved in this contract are known and are shared publicly.  All of OpeningDesign (and their consultant's) newly created documentation/content is [open source](https://creativecommons.org/licenses/by-sa/4.0/).  That is, will be freely available to you, or any party, including the design team, for future use and possible redevelopment, assuming the terms such as [Attribution](https://creativecommons.org/licenses/by-sa/4.0/) and [ShareAlike](https://creativecommons.org/licenses/by-sa/4.0/) are honored. 
  
  * **Sample Projects**
	  * A multi-family residence in Eau Claire, WI.
		  * The CAD/BIM files can be found [here](https://github.com/OpeningDesign/CTR)
		  * [Here's](https://app.element.io/#/room/!qPmKPXfEHEqedAwbst:matrix.org) a  log of communication between the design teams, and our issue tracker [here](https://github.com/OpeningDesign/CTR/issues?q=).
	  * A **sport complex** in Jefferson, WI
		  * The CAD/BIM files can be found [here](https://github.com/OpeningDesign/Sports_Complex)
		  * [Here's](https://github.com/OpeningDesign/Sports_Complex/issues?q=is:issue+is:closed+sort:comments-desc) the log of communication between the design team and general contractor
	  * A **vacation rental** in Lake Geneva, WI.
		  * The CAD/BIM documents can be found [here](https://github.com/OpeningDesign/Vacation_Rental)
		  * [Here's](https://github.com/OpeningDesign/Vacation_Rental/issues?q=is%3Aissue+is%3Aclosed+sort%3Acomments-desc) a log of communication between the design team.
	  * A **residence** on Lake Kegonsa.
		  * The CAD/BIM documents can be found [here](https://github.com/OpeningDesign/Aalseth_Residence/tree/master/Models%20%26%20CAD/BIM)


### Approach B

* **Approach (B): An Anonymous Open Approach** Everything is the same as Approach A, however, the project location and the names of the direct or indirect clients, are kept anonymous and not shared publicly.
  * **Sample Project**
    * An **organic grocery store** in southern Wisconsin 
      * (CAD/BIM) files can be found [here](https://github.com/OpeningDesign/Organic_Grocery_Store).
    * An **office/warehouse** facility in southern Wisconsin
      * (CAD/BIM) files can be found [here](https://github.com/OpeningDesign/Open_Source_Metal_Building).
        
### Approach C

* **Approach &#40;C): The Traditional Approach** where all documentation, and clients involved, remains confidential and private.   Per industry norm, the designer and the designer’s consultants are deemed the authors and owners of their respective [Instruments of Service](https://corporate.findlaw.com/intellectual-property/llp-owner-vs-architect-who-owns-the-design.html), and they retain all common law and statutory rights, including copyright.
  * With this approach, clients are free to use these construction documentations during the construction, maintenance and any future additions/modifications of their one-off project. Unlike approach A and B, however, they are not free to use these plans to redevelop another project, on another site, without OpeningDesign's permission.  Again, this is more in line with the traditional way practicing architecture and interior design.
  * A vast majority of our clients choose Approach A or B, but we provide this approach for those clients who might not, for business reasons, share their plans publicly.  For example, a data center client might opt for this more confidential/private approach, as the design of their data center is a key asset to their business model. Another example, is a client that would like to keep their project confidential until they acquire the land and/or zoning approval, then switch to a more open approach later in the project.

#### Things kept private...

> * Please note, no matter which approach (A, B, or C) is used above...
>   <!--* Any documentation from parties outside this contract, and/or shared with OpeningDesign prior and during the execution of this contract, **WILL NOT** be shared publicly.-->
>   * Any documentation you share with OpeningDesign prior and during the execution of this contract, **WILL NOT** be shared publicly.
>   * Any prior emails, or any emails between you, OpeningDesign, or any other parties during the duration of the project, **WILL NOT** be shared publicly.
>   * Also, we will not make public anything that you explicitly indicate should be kept private.

---

<!--
-->

> ##### *Although more common in other industries (software, for example), these open approaches are still relatively new in the construction industry and as such, if you have any questions please feel free to ask.  We would be happy to chat.  Ultimately we take great pride having teased out these open approaches over the years. It has proved a more efficient and cost effective way of practicing architecture--ultimately benefiting our clients and the overall industry in general.*
> 
> ##### Also, feel free to visit OpeningDesign's *[about](http://openingdesign.com/about/)* page.  It provides a little more information about these open approaches, as well.


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>





## Not-to-Exceed Fee Proposal Options (**A**, **B**, or **C**):

The following table outlines the (3) fee options. <!--The fee percentages are based on the cost of construction for (1) development.-->

<table cellspacing="0" border="0">
	<colgroup width="94"></colgroup>
	<colgroup width="236"></colgroup>
	<colgroup width="10"></colgroup>
	<colgroup span="2" width="90"></colgroup>
	<colgroup width="10"></colgroup>
	<colgroup span="2" width="90"></colgroup>
	<colgroup width="10"></colgroup>
	<colgroup span="2" width="90"></colgroup>
	<colgroup width="10"></colgroup>
	<tbody><tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" rowspan="2" height="60" align="center" valign="middle"><b><font face="Century Gothic">%</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" rowspan="2" align="center" valign="middle"><b><font face="Century Gothic">Phase</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" colspan="2" align="center" valign="middle" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic">Approach  (A)</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" colspan="2" align="center" valign="middle" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic">Approach  (B)</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" colspan="2" align="center" valign="middle" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic">Approach (C)</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic"><br></font></b></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Rough % of Const. Costs</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Estimated Duration</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Rough % of Const. Costs</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Estimated Duration</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Rough % of Const. Costs</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Estimated Duration</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic"><br></font></b></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.1" sdnum="1033;0;0.00%"><font face="Century Gothic">10.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 1: <br>Pre-Design &amp; Programming</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0016" sdnum="1033;0;0.0%"><font face="Century Gothic">0.2%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="3.2" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">3.2 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.00205" sdnum="1033;0;0.0%"><font face="Century Gothic">0.2%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="4.92" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">4.9 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0025" sdnum="1033;0;0.0%"><font face="Century Gothic">0.3%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="7.2" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">7.2 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.15" sdnum="1033;0;0.00%"><font face="Century Gothic">15.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 2: <br>Schematic Design</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0024" sdnum="1033;0;0.0%"><font face="Century Gothic">0.2%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="4.8" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">4.8 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.003075" sdnum="1033;0;0.0%"><font face="Century Gothic">0.3%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="7.38" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">7.4 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.00375" sdnum="1033;0;0.0%"><font face="Century Gothic">0.4%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="10.8" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">10.8 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.2" sdnum="1033;0;0.00%"><font face="Century Gothic">20.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 3: <br>Design Development</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0032" sdnum="1033;0;0.0%"><font face="Century Gothic">0.3%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="6.4" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">6.4 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0041" sdnum="1033;0;0.0%"><font face="Century Gothic">0.4%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="9.84" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">9.8 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.005" sdnum="1033;0;0.0%"><font face="Century Gothic">0.5%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="14.4" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">14.4 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.3" sdnum="1033;0;0.00%"><font face="Century Gothic">30.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 4: <br>Construction Documents</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0048" sdnum="1033;0;0.0%"><font face="Century Gothic">0.5%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="9.6" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">9.6 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.00615" sdnum="1033;0;0.0%"><font face="Century Gothic">0.6%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="14.76" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">14.8 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0075" sdnum="1033;0;0.0%"><font face="Century Gothic">0.8%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="21.6" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">21.6 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.05" sdnum="1033;0;0.00%"><font face="Century Gothic">5.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 5: <br>Bidding and Issuing for Permit</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0008" sdnum="1033;0;0.0%"><font face="Century Gothic">0.1%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="1.6" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">1.6 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.001025" sdnum="1033;0;0.0%"><font face="Century Gothic">0.1%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="2.46" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">2.5 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.00125" sdnum="1033;0;0.0%"><font face="Century Gothic">0.1%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="3.6" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">3.6 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.2" sdnum="1033;0;0.00%"><font face="Century Gothic">20.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 6: <br>Construction Administration</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0032" sdnum="1033;0;0.0%"><font face="Century Gothic">0.3%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">n/a</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0041" sdnum="1033;0;0.0%"><font face="Century Gothic">0.4%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">n/a</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.005" sdnum="1033;0;0.0%"><font face="Century Gothic">0.5%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">n/a</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="62" align="center" valign="middle" bgcolor="#DDDDDD" sdval="1" sdnum="1033;0;0.00%"><b><font face="Century Gothic">100.00%</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic">All Phases</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD" sdval="0.016" sdnum="1033;0;0.0%"><b><font face="Century Gothic">1.6%</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdval="25.6" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic">25.6 wks </font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD" sdval="0.0205" sdnum="1033;0;0.00%"><b><font face="Century Gothic">2.05%</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdval="39.36" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic">39.4 wks </font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD" sdval="0.025" sdnum="1033;0;0.00%"><b><font face="Century Gothic">2.50%</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdval="57.6" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic">57.6 wks </font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic"><br></font></b></td>
	</tr>
</tbody></table>


> ## Please note, these are not **lump sum** fees, but are instead, **not-to-exceed** fees, which are, in turn, based on a percentage of construction costs.  The **Hourly Rates**, called out below, will apply until this not-to-exceed fee is reached. <!--The construction costs provided here is a very rough estimate and might not align with your projected budget for the project, or the construction costs your GC might propose throughout the evolution of the project.  If the project costs go down/up, or less/more scope is involved over time, these fee ceilings will adjust relatively. -->
> 
> Please note, these not-to-exceed fees are based on the percentages of projected construction costs at any particular point in the project.  For example, if a project starts out with projected costs of 'X', but the scope is cut in half later in the project, the fees for that earlier phase will still be based on 'X'. 
> 
> We also assume your GC will share ongoing construction cost projections throughout all phases of the project.  <!--We also assume you will not act as your own GC.  -->
> 
> In the event there's a dispute over our fees or the project stops prematurely and/or changes scope before retaining a GC to determine projected construction costs, we require you to retain the services of a mutually agreed upon 3rd party estimator to determine a fair construction cost on which our not-to-exceed fee could be based.
> 
> Please note the not-to-exceed fee is applied to the entire project fee, and not the individual fees associated with each discipline and does do not include reimbursable expenses.
> 
> By using hourly rates and not-to-exceed fees, based on percentages of construction, we have found this to be a win-win for both parties.  The design professionals are given a little more safeguard against potential [scope creep](https://en.wikipedia.org/wiki/Scope_creep) and the client can realize more economical fees if they are able to make decisions quicker and more consistently--moving the design of the project along quicker.  In addition, clients are able to adjust, on the fly, what types of services they might or might not need as the project unfolds, thus avoiding tedious renegotiation.

---

<!--


## Product Markup Fee


-  To cover additional management services required to facilitate ordering, delivery, coordination and installation of the pieces, as well as addressing potential issues with the order, our fees include markup on furniture and accessories. All payments necessary to place orders are to be invoiced by Designer and to be collected in full prior to placement of the order. 
> Please Note: product markups, will not impose added cost charged to you, but rather are built-in into the retail cost of products through a discounted rate offered to us through our various vendor partners. Furthermore, with some vendors, we might be able to offer additional discounts to pass along to you -the exact discount amount, if available, will vary depending on the designer discount program offered by that particular vendor. If no trade pricing is available for the article of furniture or accessory, a 10% markup will be added to all orders procured by Designer.


-->


## Hourly Rates

<!--
> Please Note:  Services performed in Phase 1 (Programming) will be discounted **25%** discount.
-->
<table cellspacing="0" border="0">
	<colgroup width="133"></colgroup>
	<colgroup span="3" width="85"></colgroup>
	<tbody><tr>
		<td height="34" align="center" valign="middle"><b><font face="Century Gothic">Discipline</font></b></td>
		<td align="center" valign="middle"><b><font face="Century Gothic">Approach<br>(A)</font></b></td>
		<td align="center" valign="middle"><b><font face="Century Gothic">Approach<br>(B)</font></b></td>
		<td align="center" valign="middle"><b><font face="Century Gothic">Approach<br>(C)</font></b></td>
	</tr>
	<tr>
		<td colspan="4" height="17" align="left" valign="middle"><b><font face="Century Gothic">Architecture – OpeningDesign</font></b></td>
		</tr>
	<tr>
		<td height="17" align="left" valign="middle"><font face="Century Gothic">Principal</font></td>
		<td align="center" valign="middle" sdval="90" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$90/HR</font></td>
		<td align="center" valign="middle" sdval="105" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$105/HR</font></td>
		<td align="center" valign="middle" sdval="120" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$120/HR</font></td>
	</tr>
	<tr>
		<td height="17" align="left" valign="middle"><font face="Century Gothic">Project Architect</font></td>
		<td align="center" valign="middle" sdval="80" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$80/HR</font></td>
		<td align="center" valign="middle" sdval="95" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$95/HR</font></td>
		<td align="center" valign="middle" sdval="110" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$110/HR</font></td>
	</tr>
	<tr>
		<td height="17" align="left" valign="middle"><font face="Century Gothic">Intern</font></td>
		<td align="center" valign="middle" sdval="65" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$65/HR</font></td>
		<td align="center" valign="middle" sdval="80" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$80/HR</font></td>
		<td align="center" valign="middle" sdval="95" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$95/HR</font></td>
	</tr>
</tbody></table>

> ### *The Consultants fees, listed above, will include an **additional 15%** to cover in-house administration, handling, financing, and insurance costs.*

---

<!--

## The Determination of the Not-to-Exceed Fee

For your reference and peace of mind, please review the following 3rd party document(s) as to what the standard practices are for establishing design fees in the construction/architecture industry.

* [Architectural Fees (part 1)](http://www.lifeofanarchitect.com/architectural-fees-part-1/) by Bob Borson
 * [Architectural Fees…part two](http://www.lifeofanarchitect.com/architectural-fees-part-two/) by Bob Borson
 * [Architectural Fees for Residential Projects](http://www.lifeofanarchitect.com/architectural-fees-for-residential-projects/) by Bob Borson
* [A Guide to Determining Appropriate Fees  for the Services of an Architect](https://network.aia.org/HigherLogic/System/DownloadDocumentFile.ashx?DocumentFileKey=f7ea7369-a39e-4310-837d-f0d0d0c06270) from  Royal Architectural Institute of Canada

As you will see relative to the suggested fees outlined in these documents, our fees are competitive in comparison.  We are confident that through our unique and open way of working and our strong band of collaborators and consultants, that that we will meet and exceed the industry's standard of care.

-->

---

## Reimbursable expenses include:

* Transportation in connection with the project for travel authorized by the client (transportation, lodging and meals)ff
  * $1.00 per mile for travel
  * Hourly billing rates while traveling
* Communication and shipping costs (long distance charges, courier, postage, dedicated web hosting, etc.)
* Reproduction costs for plans, sketches, drawings, graphic representations and other documents
* Renderings, models, prints of computer-generated drawings, mock-ups specifically    requested     by the client
* Certification and documentation costs for third party    certification    such    as LEED
* Fees, levies, duties or taxes for permits, licenses, or approvals from authorities having jurisdiction
* Additional insurance coverage or limits, including additional professional liability insurance requested by the client in a excess of that normally carried by the designer and the designer’s consultants
* Direct expenses from additional consultants not specifically outlined in this proposal

*Reimbursable expenses include an additional 15% to cover in-house administration,    handling,    and    financing.*

---

## Boilerplate

* We will deliver invoices on a monthly basis based on scope complete, with payment due within 30 days of receipt.  Invoices overdue past (60) days will be interpreted as an order to stop work on the project and a basis for termination of contract.
* We are not responsible to select a general contractor or guarantee workmanship performed by contractor or other parties. We can, however, make suggestions or recommendations for craftsman or contractors for Client to consider
* We not responsible for the exploration, presence, handling, and/or adverse exposure of any hazardous materials, in any from. Including, but not limited to asbestos products, polychlorinated biphenyl (pcb) or other toxic substances.
* Both Parties agree to communicate about any pertinent information and known changes to the project scope in a timely manner by writing.
* In the event of premature project termination, we will bill for percentage of work completed to date, based on the enclosed hourly rate.
  
  <!--* If no site survey is supplied, this proposal does include services to change the proposed design if the base assumptions such as property line locations and setbacks, as provided by our clients, is incorrect at later stages of the design.-->
* This proposal is valid for 90 days. 

<!-- *LIMITATION OF LIABILITY
    * *In recognition of the relative risks and benefits of the Project to both the Client and the Consultant, the risks have been allocated such that the Client agrees, to the fullest extent permitted by law, to limit the liability of the Consultant and Consultant's officers, directors, partners, employees, shareholders, owners and subconsultants for any and all claims, losses, costs, damages of any nature whatsoever or claims expenses from any cause or causes, including attorneys' fees and costs and expert-witness fees and costs, so that the total aggregate liability of the Consultant and Consultants officer's, directors, partners, employees, shareholders, owners and subconsultants shall not exceed $_________, or the Consultant's total fee for services rendered on this Project, whichever is greater. It is intended that this limitation apply to any and all liability or cause of action however alleged or arising, unless 
otherwise prohibited by law.* -->

---

We sincerely appreciate the opportunity to submit this proposal and look forward to the potential of a fruitful collaboration in the future.

If I <!--we--> have included a service, within this proposal, that is not necessary and/or one that you would like to include, please let me <!--us--> know. 

<!--If the terms of this proposal are acceptable please sign in the space offered below and remit a $2000 retainer.
-->

Finally, please don't hesitate to contact me should you have any questions or need clarification about the proposal--would be more than happy to sit down and have a more nuanced discussion.

<br>

Kind Regards,

<img src="https://dl.dropboxusercontent.com/s/wwm6lvp04kvj6cn/signature.png?dl=1" width="200px"/>

Ryan Schultz
ryan.schultz@openingdesign.com
773.425.6456
17 S. Fairchild, FL 7
Madison, Wisconsin 53703

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>



### Authorized by:

* Please check the box, below, of your preferred Fee Proposal Option
  
  * **( ☐ A )  Fully Open Approach**
  * **( ☐ B ) Anonymous Open Approach**
  * **( ☐ C ) Traditional Approach** 
    
    <br>
<!--
* Or, if you prefer, you can choose the fee option per phase.  Some clients, for example, might choose option **&#40;C)** until they buy the land and/or get zoning approval , after which, switch to option **(A)**.
  
  * **Phase 1 - Programming ( ☐ A ) ( ☐ B ) ( ☐ C )**
  * **Phase 2 - Schematic Design ( ☐ A ) ( ☐ B ) ( ☐ C )**
  * **Phase 3 - Design Development ( ☐ A ) ( ☐ B ) ( ☐ C )**
  * **Phase 4 - Construction Documents ( ☐ A ) ( ☐ B ) ( ☐ C )**
  * **Phase 5 - Bidding and Construction Contract Negotiation ( ☐ A ) ( ☐ B ) ( ☐ C )**
  * **Phase 6 - Construction Administration ( ☐ A ) ( ☐ B ) ( ☐ C )**
-->
<br>

---

* Signature

---

* Title

---


* Company

---

* Date

---

<br>
<br>
<br>
<br>


###  License

Per usual, unless otherwise noted, all content associated with [OpeningDesign](http://openingdesign.com) projects is licensed under an open source, 'copyleft' license: [Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/) ![Attribution-ShareAlike 4.0 International](http://i.creativecommons.org/l/by-sa/3.0/88x31.png).  Attribution to be given to the entire team associated with the project.




<!--stackedit_data:
eyJoaXN0b3J5IjpbLTY1ODY2OTA3MiwtOTcyMTk2NDYsLTE5MD
I5MzIzODksLTQ2ODcxMDEzOSwtMTk2NDkwNzAwMywxNzgzMzU2
MTMwLC0xNzExNDU5OTA5LDIwNTg5NDEwOTUsLTE0OTcwMjkxMD
ksMTIzMTMyMjU0NSwxNjA3ODYwMCwtNDA1MTY1NzA2LDcwNjUz
NjI0OSw5NzU5NjQ4NywtMjAzOTIwMjI4Nyw2NTQ3MTk4OTksLT
MxMTIxODQzOCwxNjEwMDIwNDAwLC02NzgzNDc1MzMsLTE1NzA5
MDA5MThdfQ==
-->

<!--stackedit_data:
eyJoaXN0b3J5IjpbMjA1OTQ1NzY5NCw4NTI3NTUwOTYsMTM2OT
Y0ODIxMCwtMjA2NTcwMTczMSwtMTY5NzM0ODE5MCwtNjU4NzYy
MzgxLC05MjMyMzE3OTgsMTg3MjgxMTUzNiwtMTE3NDU4NjEzOC
w2NjA2ODg4OTAsLTYyNDUxNzYzOCwtNjI0NTE3NjM4LDEzOTQ4
OTMyNjIsLTE4MzE4MTk2ODgsLTY5OTM4MTcxOCwxNTUwODYwNT
I1LDMwNzk0NzU5NCwtMTMyMDA0MDk2NCwtMTU4ODU5ODI3Niwt
MTQzMzY0OTg1NF19
-->