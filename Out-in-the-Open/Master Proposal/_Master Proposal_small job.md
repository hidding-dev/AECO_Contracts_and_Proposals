<div align="right" >
<p align="right" >
<img src="https://raw.githubusercontent.com/OpeningDesign/OD_Marketing/master/Logos/od_icon_logo_2.jpg" width="120px"/>
</p>
</div>

<!--
<div align="right" >
<p align="right" >
<img src="https://dl.dropbox.com/s/64fehzmn6sfaq1g/PHD%20logo.png?dl=1" width="300px"/>
</p>
</div>
-->

### Proposal for [OpeningDesign's](http://openingdesign.com/) Architecture & Structural Engineering<!--& Interior Design Engineering--> Services

<!--

### Proposal for the following services<!--[OpeningDesign's](http://openingdesign.com/) 
- Interior Design
- Furniture, Fixtures, and Equipment (FF&E) Selection and Procurement
- Architecture and Engineering

-->

---

FOR:

>Scott Kluetzman  
> S&J's Task Masters

---



Hi Scott,

Thank you for the chat the other day. We’re excited to have the opportunity to share the following proposal.

Although a more nuanced list of requirements will undoubtedly unfold as the project evolves, on a high-level, I understand that this project will include the following list of requirements/priorities.

- Project includes a 1 story addition on to an existing home at 4013 Mineral Point Rd, Madison, WI 53705
- Addition to incorporate
	- (1) bedroom
		- (1) bath
	- (1) office
- Kitchen renovation
- renovated 1/2 bathroom and laundry room
- Backyard deck
- Frontyard stairs and entry stoop


The following is a breakdown of services and phases we anticipate for your project.

---



## Phases & Scope of Services

### **Phase 1 - Programming** <!--*(Fees associated with this phase are discounted at 25%)*-->
  
  <!--* Architecture-->
  
  * Conduct zoning & building code analysis
   * Measure, model, and draft up the existing conditions relative to the scope of work
	

### **Phase 2 - Schematic Design**
  
 * Architecture & Interior Design
     * Floor Plans
     * Interior Elevations - key areas only
     * Building Sections
     * Elevations
	     * Of the scope area only, not the entire building



### **Phase 3 - Design Development**

   * Architecture & Interior Design
        * Floor Plans
        * Demolition plans, if applicable
        * Exterior Elevations
            * Of the scope area only, not the entire building
        * Interior Elevations - key areas only
        * Building Sections
   



### **Phase 4 - Construction Documents**
  
   * Architecture & Interior Design
       <!--* Code summary
       * ADA requirements
       * Life safety plan
       * Emergency and exit sign layouts-->
       * Site Plan
       * Demolition plans
       * Floor plans
       * Roof Plan
       * Building Sections
       * Building Elevations
           <!--* Of the scope area only, not the entire building-->
       * Wall Sections
    * Structural
	    * Floor plans with structural member sizes
	    * Structural calculations, if necessary



### **Phase 5 - Bidding and Issuing for Permit**
  
    
   <!--* If necessary, a bidding set to a predetermined list of GCs
  * Answer GC & subcontractor's bid questions, issue clarifications-->
  * Issue the drawings for building permit review
    * Provide the city/town/village, reviewing the drawings, with any additional drawings and/or clarifications they may request after their initial review.  This happens quite often and is a normal part of the process.

> ##### Post Permit Changes
> 
> There are times our clients will make changes relatively late in the process (Phase 4 and beyond). If small enough and done early enough, we can usually subsume them into the base bid.  However, if they become big enough and too late in the process, we will let you know and issue a not-to-exceed change order, for your approval, before any work is done.  
> 
> If you decide at a later time to reissue for a revised permit with these changes, any additional work required by the AHJ for this revised permit is not included in the base bid and will be charged at an hourly rate per the enclosed billing rate schedule.
> 
> Although we will make every effort to make these post-permit changes code compliant, if you instruct your GC to implement these changes before receiving the revised permit, OpeningDesign can not assume any liability for any rework that might result from drawings that are not code compliant upon re-review.  Since the building code is multifaceted and, at times, open to local interpretation, we require the approval of the governing body (AHJ) to manage our risk of liability.  **Please note, this applies to any construction done before the first permit issuance, as well.**



### **Phase 6 - Construction Administration**

* Architecture & Interior Design
	* Site visits
		* Assuming 1 every 3 weeks (additional site visits, if necessary, will be billed at hourly rates outlined in this proposal, including mileage costs)
	* Review/Respond to the following GC inquiries
      * RFIs (request for information)
      * Shop drawings and submittals
      * Material/Product substitutions proposed by the GC
      * Change orders
      * Develop punch list, if necessary
       

<br>

> ##### Termination
> When construction starts, our job is not over.  Our involvement during this phase is typically just as important as the earlier phases, as quite often, due to accelerated schedules, we continue to work out the remaining details of the design.
> 
> In addition, our periodic on-site observation allows us to catch any construction errors and/or quality problems.
> 
>  Having said this, you are free to stop our services at any time, and at any phase, however, as our involvement is crucial in all the above phases, if you terminate our service before the end of construction, we would be required to remove our name as the architect/engineer of record from the AHJ, and would no longer be professionally liable for any errors and omissions that might be exposed later in the project.  Our involvement throughout the project is crucial to minimizes our liability, as well as yours.



---

## Services *not* included:

Although **we can provide** the following services, we assume either they are not necessary or will be provided by a 3rd party via the GC or directly contracted through you.

<!--Although we can provide services in both Tiers, for those services in the **1st Tier**, we can provide the most value specific to your project. -->

*Please lets us know if you would like us to include any of the following.*

   * As-built drawings for areas outside the scope.
	    * Sometimes an AHJ will request additional floor plans outside the scope area.  This would include drafting up the 2nd floor or basement, for example.
  * Civil engineering
       * Storm water management/calculations
       * Grading and erosion control plan/details
        * Site stabilization details/methods.
        * Sediment control measures
        * Retaining wall design
  * HVAC/Mechanical Design
  * Electrical Design
  * Lighting
  * Plumbing Design
  * Food Service or Commercial Kitchen Design
    * Kitchen equipment procurement
     * We will, however, suggest a schematic kitchen layout intended to be refined by a 3rd party kitchen vendor.
  * Furniture, Fixtures & Equipment (FF&E) services
  * Interior design
	      * Picking out interior finishes
	  * Casework/millwork elevations
	    * [Large scale](http://openingdetail.com/gallery/) casework/millwork details
  * [Large scale](http://openingdetail.com/gallery/) interior details
  * Long form (book) construction specifications
  * Landscape design
  * Structural work associated with shoring during construction
  * Land Surveying
  * Low Voltage Design
  * Audio/Visual Design
  * Extensive energy modeling beyond prescriptive requirements
  * LEED Design
  * LEED Commissioning
  * Hyper-Realistic Renderings
  * Detailed Cost Estimation
  * Fire Alarm
  * Fire Protection (Sprinklers)
  * Security
  * Signage Design/Layout
  * Acoustical Engineering Services
  * Geotechnical Engineering
  * Environmental Studies and Reports
  * Materials testing or inspections
  * Information Technology
  * Legal Survey
  * Closeout Record Documents
  * Testing and Balancing of Installed Equipment
  * Environmental Studies
  * Commissioning Services
  * Moving Coordination
  * Post-occupancy Elevation/Studies
  * Maintenance and Operational Programming
  * Building Maintenance Manuals
  * Post-occupancy Evaluation
  * Extensive decorative finish studies
  * Art selection
  * Artwork Production Services 
  * Additional art commission services may be provided to client and quoted separately




---








## Not-to-Exceed Fee:

<table cellspacing="0" border="0">
	<colgroup width="94"></colgroup>
	<colgroup width="236"></colgroup>
	<colgroup span="2" width="90"></colgroup>
	<colgroup width="10"></colgroup>
	<tbody><tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="42" align="center" valign="middle"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Phase</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic">Not-To-Exceed</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Estimated Duration</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic"><br></font></b></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.05" sdnum="1033;0;0.00%"><font face="Century Gothic">5.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 1: <br>Pre-Design &amp; Programming</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="400" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$400</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="0.625" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">0.6 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.1" sdnum="1033;0;0.00%"><font face="Century Gothic">10.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 2: <br>Schematic Design</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="800" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$800</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="1.25" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">1.3 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.3" sdnum="1033;0;0.00%"><font face="Century Gothic">30.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 3: <br>Design Development</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="2400" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$2,400</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="3.75" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">3.8 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.3" sdnum="1033;0;0.00%"><font face="Century Gothic">30.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 4: <br>Construction Documents</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="2400" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$2,400</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="3.75" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">3.8 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.05" sdnum="1033;0;0.00%"><font face="Century Gothic">5.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 5: <br>Bidding and Issuing for Permit</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="400" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$400</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="0.625" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">0.6 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.2" sdnum="1033;0;0.00%"><font face="Century Gothic">20.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 6: <br>Construction Administration</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="1600" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$1,600</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">n/a</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="62" align="center" valign="middle" bgcolor="#DDDDDD" sdval="1" sdnum="1033;0;0.00%"><b><font face="Century Gothic">100.00%</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic">All Phases</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD" sdval="8000" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic">$8,000</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdval="10" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic">10.0 wks </font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic"><br></font></b></td>
	</tr>
</tbody></table>


> Please note, these are not **lump sum** fees, but are instead, **not-to-exceed** fees.  The **Hourly Rates**, called out below, will apply until this not-to-exceed fee is reached. 

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

## Hourly Rates

<!--
> Please Note:  Services performed in Phase 1 (Programming) will be discounted **25%** discount.
-->
<table cellspacing="0" border="0">
	<colgroup width="146"></colgroup>
	<colgroup span="4" width="85"></colgroup>
	<tbody><tr>
		<td height="66" align="center" valign="middle"><b><font face="Century Gothic">Discipline</font></b></td>
		<td align="center" valign="middle"><b><font face="Century Gothic">Approach<br>(A)</font></b></td>
		<td align="center" valign="middle"><b><font face="Century Gothic">Approach<br>(B)</font></b></td>
		<td align="center" valign="middle"><b><font face="Century Gothic">Approach<br>(C)</font></b></td>
		<td align="center" valign="middle" sdnum="1033;0;0.0%"><b><font face="Century Gothic">A Rough Estimated Percentage of Fees</font></b></td>
	</tr>
	<tr>
		<td colspan="4" height="17" align="left" valign="middle"><b><font face="Century Gothic">Architecture – OpeningDesign</font></b></td>
		<td align="center" valign="middle" sdval="0.857142857142857" sdnum="1033;0;0%"><font face="Century Gothic">86%</font></td>
	</tr>
	<tr>
		<td height="17" align="left" valign="middle"><font face="Century Gothic">Principal</font></td>
		<td align="center" valign="middle" sdval="90" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$90/HR</font></td>
		<td align="center" valign="middle" sdval="105" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$105/HR</font></td>
		<td align="center" valign="middle" sdval="120" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$120/HR</font></td>
		<td align="center" valign="middle" sdnum="1033;0;0%"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td height="17" align="left" valign="middle"><font face="Century Gothic">Project Architect</font></td>
		<td align="center" valign="middle" sdval="80" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$80/HR</font></td>
		<td align="center" valign="middle" sdval="95" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$95/HR</font></td>
		<td align="center" valign="middle" sdval="110" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$110/HR</font></td>
		<td align="center" valign="middle" sdnum="1033;0;0%"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td height="17" align="left" valign="middle"><font face="Century Gothic">Intern</font></td>
		<td align="center" valign="middle" sdval="65" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$65/HR</font></td>
		<td align="center" valign="middle" sdval="80" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$80/HR</font></td>
		<td align="center" valign="middle" sdval="95" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$95/HR</font></td>
		<td align="center" valign="middle" sdnum="1033;0;0%"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td colspan="4" height="17" align="left" valign="middle"><b><font face="Century Gothic">Structural Engineer</font></b></td>
		<td align="center" valign="middle" sdval="0.142857142857143" sdnum="1033;0;0%"><font face="Century Gothic">14%</font></td>
	</tr>
	<tr>
		<td height="17" align="left" valign="middle"><font face="Century Gothic">Engineer I</font></td>
		<td align="center" valign="middle" sdval="180" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$180/HR</font></td>
		<td align="center" valign="middle" sdval="195" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$195/HR</font></td>
		<td align="center" valign="middle" sdval="210" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$210/HR</font></td>
		<td align="center" valign="middle" sdnum="1033;0;0%"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td height="17" align="left" valign="middle"><font face="Century Gothic">Junior Drafter</font></td>
		<td align="center" valign="middle" sdval="65" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$65/HR</font></td>
		<td align="center" valign="middle" sdval="80" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$80/HR</font></td>
		<td align="center" valign="middle" sdval="95" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$95/HR</font></td>
		<td align="center" valign="middle" sdnum="1033;0;0%"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td height="17" align="left" valign="middle"><font face="Century Gothic">Junior Drafter</font></td>
		<td align="center" valign="middle" sdval="65" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$65/HR</font></td>
		<td align="center" valign="middle" sdval="80" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$80/HR</font></td>
		<td align="center" valign="middle" sdval="95" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$95/HR</font></td>
		<td align="center" valign="middle" sdnum="1033;0;0%"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td colspan="4" height="18" align="right" valign="middle"><b><font face="Century Gothic">Totals</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdval="1" sdnum="1033;0;0%"><b><font face="Century Gothic">100%</font></b></td>
	</tr>
</tbody></table>

> ### *The Consultants fees, listed above, will include an **additional 15%** to cover in-house administration, handling, financing, and insurance costs.*

---

<!--

## The Determination of the Not-to-Exceed Fee

For your reference and peace of mind, please review the following 3rd party document(s) as to what the standard practices are for establishing design fees in the construction/architecture industry.

* [Architectural Fees (part 1)](http://www.lifeofanarchitect.com/architectural-fees-part-1/) by Bob Borson
 * [Architectural Fees…part two](http://www.lifeofanarchitect.com/architectural-fees-part-two/) by Bob Borson
 * [Architectural Fees for Residential Projects](http://www.lifeofanarchitect.com/architectural-fees-for-residential-projects/) by Bob Borson
* [A Guide to Determining Appropriate Fees  for the Services of an Architect](https://network.aia.org/HigherLogic/System/DownloadDocumentFile.ashx?DocumentFileKey=f7ea7369-a39e-4310-837d-f0d0d0c06270) from  Royal Architectural Institute of Canada

As you will see relative to the suggested fees outlined in these documents, our fees are competitive in comparison.  We are confident that through our unique and open way of working and our strong band of collaborators and consultants, that that we will meet and exceed the industry's standard of care.

-->

---

## Reimbursable expenses include:

* Transportation in connection with the project for travel authorized by the client (transportation, lodging and meals)ff
  * $1.00 per mile for travel
  * Hourly billing rates while traveling
* Communication and shipping costs (long distance charges, courier, postage, dedicated web hosting, etc.)
* Reproduction costs for plans, sketches, drawings, graphic representations and other documents
* Renderings, models, prints of computer-generated drawings, mock-ups specifically    requested     by the client
* Certification and documentation costs for third party    certification    such    as LEED
* Fees, levies, duties or taxes for permits, licenses, or approvals from authorities having jurisdiction
* Additional insurance coverage or limits, including additional professional liability insurance requested by the client in a excess of that normally carried by the designer and the designer’s consultants
* Direct expenses from additional consultants not specifically outlined in this proposal

*Reimbursable expenses include an additional 15% to cover in-house administration,    handling,    and    financing.*

---

## Boilerplate

* We will deliver invoices on a monthly basis based on scope complete, with payment due within 30 days of receipt.  Invoices overdue past (60) days will be interpreted as an order to stop work on the project and a basis for termination of contract.
* We are not responsible to select a general contractor or guarantee workmanship performed by contractor or other parties. We can, however, make suggestions or recommendations for craftsman or contractors for Client to consider
* We not responsible for the exploration, presence, handling, and/or adverse exposure of any hazardous materials, in any from. Including, but not limited to asbestos products, polychlorinated biphenyl (pcb) or other toxic substances.
* Both Parties agree to communicate about any pertinent information and known changes to the project scope in a timely manner by writing.
* In the event of premature project termination, we will bill for percentage of work completed to date, based on the enclosed hourly rate.
  
  <!--* If no site survey is supplied, this proposal does include services to change the proposed design if the base assumptions such as property line locations and setbacks, as provided by our clients, is incorrect at later stages of the design.-->
* This proposal is valid for 90 days. 

<!-- *LIMITATION OF LIABILITY
    * *In recognition of the relative risks and benefits of the Project to both the Client and the Consultant, the risks have been allocated such that the Client agrees, to the fullest extent permitted by law, to limit the liability of the Consultant and Consultant's officers, directors, partners, employees, shareholders, owners and subconsultants for any and all claims, losses, costs, damages of any nature whatsoever or claims expenses from any cause or causes, including attorneys' fees and costs and expert-witness fees and costs, so that the total aggregate liability of the Consultant and Consultants officer's, directors, partners, employees, shareholders, owners and subconsultants shall not exceed $_________, or the Consultant's total fee for services rendered on this Project, whichever is greater. It is intended that this limitation apply to any and all liability or cause of action however alleged or arising, unless 
otherwise prohibited by law.* -->

---

We sincerely appreciate the opportunity to submit this proposal and look forward to the potential of a fruitful collaboration in the future.

If I <!--we--> have included a service, within this proposal, that is not necessary and/or one that you would like to include, please let me <!--us--> know. 

<!--If the terms of this proposal are acceptable please sign in the space offered below and remit a $2000 retainer.
-->

Finally, please don't hesitate to contact me should you have any questions or need clarification about the proposal--would be more than happy to sit down and have a more nuanced discussion.

<br>

Kind Regards,

<img src="https://dl.dropboxusercontent.com/s/wwm6lvp04kvj6cn/signature.png?dl=1" width="200px"/>

Ryan Schultz
ryan.schultz@openingdesign.com
773.425.6456
17 S. Fairchild, FL 7
Madison, Wisconsin 53703

<br>
<br>
<br>



### Authorized by:



<!--
* Or, if you prefer, you can choose the fee option per phase.  Some clients, for example, might choose option **&#40;C)** until they buy the land and/or get zoning approval , after which, switch to option **(A)**.
  
  * **Phase 1 - Programming ( ☐ A ) ( ☐ B ) ( ☐ C )**
  * **Phase 2 - Schematic Design ( ☐ A ) ( ☐ B ) ( ☐ C )**
  * **Phase 3 - Design Development ( ☐ A ) ( ☐ B ) ( ☐ C )**
  * **Phase 4 - Construction Documents ( ☐ A ) ( ☐ B ) ( ☐ C )**
  * **Phase 5 - Bidding and Construction Contract Negotiation ( ☐ A ) ( ☐ B ) ( ☐ C )**
  * **Phase 6 - Construction Administration ( ☐ A ) ( ☐ B ) ( ☐ C )**
-->
<br>

---

* Signature

---

* Title

---


* Company

---

* Date

---

<br>
<br>
<br>
<br>


###  License

Per usual, unless otherwise noted, all content associated with [OpeningDesign](http://openingdesign.com) projects is licensed under an open source, 'copyleft' license: [Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/) ![Attribution-ShareAlike 4.0 International](http://i.creativecommons.org/l/by-sa/3.0/88x31.png).  Attribution to be given to the entire team associated with the project.




<!--stackedit_data:
eyJoaXN0b3J5IjpbLTY1ODY2OTA3MiwtOTcyMTk2NDYsLTE5MD
I5MzIzODksLTQ2ODcxMDEzOSwtMTk2NDkwNzAwMywxNzgzMzU2
MTMwLC0xNzExNDU5OTA5LDIwNTg5NDEwOTUsLTE0OTcwMjkxMD
ksMTIzMTMyMjU0NSwxNjA3ODYwMCwtNDA1MTY1NzA2LDcwNjUz
NjI0OSw5NzU5NjQ4NywtMjAzOTIwMjI4Nyw2NTQ3MTk4OTksLT
MxMTIxODQzOCwxNjEwMDIwNDAwLC02NzgzNDc1MzMsLTE1NzA5
MDA5MThdfQ==
-->

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEzOTcwMzQ2OTIsMTQ4MzUwOTc2MCwxNT
g5NDIyNzk4LDEzMjgzMzcxNTYsMTE4MDY5MDEwNiwtNTgwMDY5
NDgzLDE4NDIwODg2MTMsLTE2ODgzNjU2MzAsLTE0MzM2NDk4NT
QsLTIwOTI3MDQ5ODUsMTcxMDIyOTgzNywtMTM3NDk5NDM1Miwt
MTU5MDA5MDkyNywtMTc2MjcwMjQ3OCw4NDY5MTg3NjAsMjk5NT
IzMDk3LC00ODAxNTMyMTUsLTIxMzUzNTA2MTYsMTk2MzE2MzQ2
LC0xNzE1OTU4OTcyXX0=
-->